# NeringaTour API
**API URL: https://neringatour.lt/api**

**API Header: application/x.neringatour.v3+json**

**Naudotojo prisijungimui naudojamas https://auth0.com**

## Gaunama duomenu bazes versija [GET /get_db_version]


## Gaunami aplikacijos nustatymai [GET /get_app_settings]


## Gaunamos visos naujienos [GET /get_all_news{?language}]


+ Parameters
    + language: (string, optional) - Pageidaujama kalba (lt arba en)
        + Default: lt

## Gaunami visi renginiai [GET /get_all_events{?language}]


+ Parameters
    + language: (string, optional) - Pageidaujama kalba (lt arba en)
        + Default: lt

## Gaunamos visos vietu kategorijos [GET /get_all_categories{?language}]


+ Parameters
    + language: (string, optional) - Pageidaujama kalba (lt arba en)
        + Default: lt

## Gaunamos visos vietos [GET /get_all_places{?language}]


+ Parameters
    + language: (string, optional) - Pageidaujama kalba (lt arba en)
        + Default: lt

## Gaunamas autobusu tvarkastis [GET /get_bus_schedule]


## Gaunama hidrologine informacija [GET /get_water_data{?language}]
Priklausomai nuo sezono rodoma vandens telkiniu temperatura arba ledo storis

+ Parameters
    + language: (string, optional) - Pageidaujama kalba (lt arba en)
        + Default: lt

## Gaunama vieta pagal ID [GET /get_place/{id}{?language}]


+ Parameters
    + id: (integer, required) - Vietos ID
    + language: (string, optional) - Pageidaujama kalba (lt arba en)
        + Default: lt

## Gaunamas renginys pagal ID [GET /get_event{id}{?language}]


+ Parameters
    + id: (integer, required) - Renginio ID
    + language: (string, optional) - Pageidaujama kalba (lt arba en)
        + Default: lt

## Gaunama naujiena pagal ID [GET /get_news/{id}{?language}]


+ Parameters
    + id: (integer, required) - Naujienos ID
    + language: (string, optional) - Pageidaujama kalba (lt arba en)
        + Default: lt

## Siunciamas vietos atsiliepimas [POST /post_place_review]


+ Request (application/json)
    + Body

            {
                "place_id": "integer",
                "device_id": "string",
                "review": "string"
            }

+ Request (application/json)
    + Headers

            Authorization: Bearer {authorization token}

## Siunciamas vietos reitingas [POST /post_place_rating]
Grazinamas naujas vietos reitingas bei balsu skaicius

+ Request (application/json)
    + Body

            {
                "place_id": "integer",
                "device_id": "string",
                "rating": "integer (0-5)"
            }

+ Response 200 (application/json)
    + Body

            {
                "rating": "float",
                "total_votes": "integer"
            }